
import { AfterViewInit, Component, OnInit, ViewChild, NgModule } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { MatDialog } from '@angular/material';
import Teacher from 'src/app/entity/teacher';
import { ActivityTableDataSource } from 'src/app/students/student-dashboard/student-dashboard-datasource';
@Component({
  selector: 'app-teacher-approve',
  templateUrl: './teacher-approve.component.html',
  styleUrls: ['./teacher-approve.component.css']
})
export class TeacherApproveComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;
  

  ngAfterViewInit(){
  }

  displayedColumns: string[] = ['activityId', 'activityImage','activityName', 'location', 'date', 'time',
   'regisDateStart', 'TeacherName', 'description','enrollButton'];
  activities: Activity[];
  Teacher: Teacher[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService, public dialog: MatDialog) { }
  
 
  ngOnInit() {
    this.activityService.getAllActivities()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }

  // provide diaglog when click descripton button
  

  // openDialog(id: any): void {
  //   console.log(id);
  //   const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
  //     width: '350px',
  //     data: "Are you sure to confirm the enrollment to this activity?"
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     if(result) {
  //       console.log('Yes clicked');
        // DO SOMETHING
        
  //       if(id == '002'){
  //         console.log(document.getElementById("enrollBtn002"));
  //         document.getElementById("enrollBtn002").innerHTML = "PENDING!!!";
  //       }if(id == '001'){
  //         console.log(document.getElementById("enrollBtn001"));
  //         document.getElementById("enrollBtn001").innerHTML = "PENDING!!!";
  //       }if(id == '003'){
  //         console.log(document.getElementById("enrollBtn003"));
  //         document.getElementById("enrollBtn003").innerHTML = "PENDING!!!";
  //       }
        
  //     }

  //   });
  // }
  
  applyFilter(filterValue: any) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
