import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';
import { SimpleDialogComponent } from 'src/app/simple-dialog/simple-dialog.component';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity-service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ActivityTableDataSource } from 'src/app/students/student-dashboard/student-dashboard-datasource';
@Component({
  selector: 'app-teacher-dashboard',
  templateUrl: './teacher-dashboard.component.html',
  styleUrls: ['./teacher-dashboard.component.css']
})
export class TeacherDashboardComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;
  

  ngAfterViewInit(){
  }

  
  displayedColumns: string[] = [
    'activityId', 
    'activityImage',
    'activityName', 
    'location', 
    'date', 
    'time', 
    'regisDateStart', 
    'TeacherName', 
    'description',
    'enrollButton'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService,public dialog: MatDialog, private authService:AuthenticationService) { }

  hasRole(role: string){
    return this.authService.hasRole(role);
  }
  get user(){
    return this.authService.getCurrentUser();
  }
  
 
  ngOnInit() {
    this.activityService.getAllActivities()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );

  }

  activity:Activity
  applyFilter(filterValue: any) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
}

