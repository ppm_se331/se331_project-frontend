import { Component, OnInit, ViewChild } from '@angular/core';
import Activity from 'src/app/entity/activity';
import { Params, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { environment } from 'src/environments/environment';
import { Validators, FormBuilder } from '@angular/forms';
import { FileUploadService } from 'src/app/service/file-upload.service';
import { ActivityRestImplService } from 'src/app/service/activity-rest-impl.service';
import { Image } from 'src/app/entity/Image'
import { UserService } from 'src/app/service/user-servicce';
import { Comment } from 'src/app/entity/comment';
import { User } from 'src/app/entity/user';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { ActivityService } from 'src/app/service/activity-service';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  
  activities: Activity[];
  ngAfterViewInit(){
  }

defaultImageUrl = 'assets/images/camt.jpg';
  uploadEndpoint: string;
  uploadedUrl: string;
  progress: number;
  activity:Activity;
  images: Image[] = [];
  User: User;
  isUploaded = false;
  isUploadingComment: Boolean = false;
  commentForm = this.fb.group({
    comment: [null, Validators.required],
    image: ['']
  });

  constructor(private route: ActivatedRoute,
    private activityService: ActivityService,
    private fileUploadService: FileUploadService,
    private authService: AuthenticationService,
    private activityRestService:ActivityRestImplService,
    private userService:UserService,
    private fb:FormBuilder
    ) { }
    
  ngOnInit() {

    this.route.params.subscribe((params:Params) => {
      this.activityRestService.getActivity(+params['id'])
      .subscribe((activity:Activity) => {
        this.activity = activity;
        console.log(this.activity);
        if( this.activity.comments){
          this.activity.comments.forEach(element => {
            this.userService.getUserImageByUserId(element.user.id)
            .subscribe(returnImg => {
              element.userImage = returnImg.image;
            });
          });
        }
      });
    })

    console.log(this.images);
    this.uploadEndpoint = environment.uploadApi;
  }

  get user() {
    return this.authService.getCurrentUser();
  }

  get role(){
    return this.authService.role();
  }
  onUploadClicked(files?:FileList) {
    console.log(typeof(files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe( (event:HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Sent:
              console.log('Request has been made!');
              break;
            case HttpEventType.ResponseHeader:
              console.log('Response header has been received!');
              break;
            case HttpEventType.UploadProgress:
              this.progress = Math.round(event.loaded / event.total * 100);
              console.log(`Uploaded! ${this.progress}%`);
              break;
            case HttpEventType.Response:
              console.log('User successfully created!', event.body);
              this.uploadedUrl = event.body;
              this.commentForm.patchValue( {
                image: this.uploadedUrl
              });
              this.commentForm.get('image').updateValueAndValidity();
                setTimeout( () => {
                  this.progress = 0;
                  this.isUploaded = true;
                },1500);
          }
      });
  }
  onSelectedFilesChanged(files?:FileList) {}

  addComment() {
    this.isUploadingComment = true;
    var comment: Comment = new Comment();
    comment.user = this.authService.getCurrentUser();
    comment.text  = this.commentForm.value['comment'];
    if(this.commentForm.value['image']){
      comment.image = this.commentForm.value['image'];
    } else {
      comment.image = null;
    }
    comment.activity = this.activity;
    console.log("Image URL: " + this.commentForm.value.image)
    this.activityRestService.addComment(comment).subscribe(activity => {
      console.log(activity);
      setTimeout(() => {
        this.isUploadingComment = false;
      }, 500)
      window.location.reload();
    });
    console.log(this.commentForm.value['comment']);
  }


  deleteComment(comment: Comment) {
    console.log("delete!!!");
    console.log(comment.id);
    this.activityRestService.deleteComment(comment).subscribe(activity => {
      console.log(activity);
      window.location.reload();
    });
  }


}
