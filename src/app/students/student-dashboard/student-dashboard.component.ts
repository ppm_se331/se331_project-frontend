import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import Student from '../../entity/student';
import { SimpleDialogComponent } from '../../simple-dialog/simple-dialog.component';
import { CommentComponent } from '../comment/comment.component';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service';
import { ActivityTableDataSource } from './student-dashboard-datasource';
import { StudentService } from 'src/app/service/student-service';
import Teacher from 'src/app/entity/teacher';

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.component.html',
  styleUrls: ['./student-dashboard.component.css']
})
export class StudentDashboardComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;
  

  ngAfterViewInit(){
  }

  
  displayedColumns: string[] = [
    'activityId', 
    'activityImage',
    'activityName', 
    'location', 
    'date', 
    'time', 
    'regisDateStart', 
    'TeacherName', 
    'description',
    'enrollButton',
    'status'];
    loading: boolean;
    activities: Activity[];
    students:Student[];
    teacher:Teacher[];
    uploadEndPoint: string;
    isRegistered: boolean = false;
    filter: string;
    filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService,public dialog: MatDialog, private authService:AuthenticationService,private router: Router,
    private studentService:StudentService) { }

  hasRole(role: string){
    return this.authService.hasRole(role);
  }
  get user(){
    return this.authService.getCurrentUser();
  }

  openSimpleDialog() {
    this.dialog.open(CommentComponent, {
         width: '90rem',
         height: '40rem'
    });
}
  
 
  ngOnInit() {
    this.activityService.getAllActivities()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );

  }

  activity:Activity
  applyFilter(filterValue: any) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  get allActivities(){
    return this.authService.getCurrentUser();
  }
  get lecturer() {
    return this.authService.getCurrentUser();
  }

  routeToActivityInfo(activityId: number) {
    this.router.navigate(['activity/info',activityId]);
  }

  dateFormat(date: number) {
    if (date.toString().length == 1) {
      return "0" + date;
    } else {
      return date;
    }
  }

  date(date) {
    var d = new Date(date);
     return this.dateFormat(d.getDate()) + "/" + this.dateFormat(d.getMonth() + 1) + "/" + d.getFullYear();
  }
  
  enrolled(activity:Activity){
    var Enrolled = false;
    activity.students.forEach(element =>{
      
      if(element.id == this.authService.getCurrentUser().id){
        Enrolled = true;
      }
      
    });
    activity.students.forEach(element => {
      if(element.id == this.authService.getCurrentUser().id){
        Enrolled = true;
      }
    });
    return Enrolled;
  }

  enrollactivities(activity: Activity){
    const User = this.authService.getCurrentUser();
    console.log(this.authService.getCurrentUser());
    activity.students.forEach(element => {
      if(element.id == this.authService.getCurrentUser().id) {
        console.log("true")
        this.isRegistered = true;
      }
    });
    if(this.isRegistered){
    } else {
      var isOk = confirm("Are you sure to enrolled in " + activity.activityName + " activity ?");
      if(isOk){
        activity.pendingActivities.push(User);
        console.log(activity);
        this.activityService.saveActivity(activity).subscribe((data) => {
        });
      }
    }
    this.isRegistered = false;
  }
}