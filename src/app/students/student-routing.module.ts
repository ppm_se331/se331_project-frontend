import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentsComponent } from './list/students.component';
import { NgModule } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { LecturerGuard } from '../Guard/lecturer.guard';
import { StudentAndteacherGuard } from '../Guard/student-and-lecturer.guard';
import { StudentEnrollmentComponent } from './student-enrollment/student-enrollment.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { CommentComponent } from './comment/comment.component';
import { TeacherApproveComponent } from '../teacher/teacher-approve/teacher-approve.component';



const StudentRoutes: Routes = [
    { path: 'dashboard', component: StudentDashboardComponent ,canActivate:[StudentAndteacherGuard]},
    { path: 'enrollment', component: StudentEnrollmentComponent ,canActivate:[StudentAndteacherGuard]},
    { path: 'DescriptionActivity/:id', component: CommentComponent ,canActivate:[StudentAndteacherGuard]},
    { path: 'table', component: StudentTableComponent ,canActivate:[StudentAndteacherGuard]},
];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}
