import { Component, OnInit } from '@angular/core';
import Student from '../entity/student';
import { Validators, FormBuilder } from '../../../node_modules/@angular/forms';
import { environment } from '../../environments/environment';
import { HttpEvent, HttpEventType } from '../../../node_modules/@angular/common/http';
import { Router } from '../../../node_modules/@angular/router';
import { FileUploadService } from '../service/file-upload.service';
import { StudentRestImplService } from '../service/student-rest-impl.service';
import { AuthenticationService } from '../service/authentication.service';
import { User } from '../entity/user';

@Component({
  selector: 'app-update-profile-dialog',
  templateUrl: './update-profile-dialog.component.html',
  styleUrls: ['./update-profile-dialog.component.css']
})

export class UpdateProfileDialogComponent {


  students: Student[];
  emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  hide = true;

  form = this.fb.group({
    id: [''],
    // studentId: [null, Validators.compose([Validators.required, Validators.pattern(/^\d{9}$/)])],
    name: [null, Validators.compose([Validators.required, Validators.pattern(/^[A-Za-z]{4,50}$/)])],
    surname: [null, Validators.compose([Validators.required, Validators.pattern(/^[A-Za-z]{4,50}$/)])],
    // dob: [null, Validators.required],
    image: [''],
    // email: [null, Validators.compose([Validators.required, Validators.pattern(this.emailRegex)])],
    password: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
    passwordConf: [null, Validators.required]
  });


  //validate input form
  validation_messages = {
    'studentId': [
      { type: 'required', message: 'student id is required' },
      { type: 'pattern', message: 'student id must be a 9 digits number' }
    ],
    'name': [
      { type: 'required', message: 'the name is required' },
      { type: 'pattern', message: 'the name is required' }
    ],
    'surname': [
      { type: 'required', message: 'the surname is required' },
      { type: 'pattern', message: 'the surname is required' }
    ]
    , 'email': [
      { type: 'required', message: 'the email is required' },
      { type: 'pattern', message: 'the email is incorrect' }
    ]
    , 'password': [
      { type: 'required', message: 'the password is required' },
      { type: 'pattern', message: 'the password is too long' }
    ]
    , 'passwordConf': [
      { type: 'required', message: 'the password confirmation is required' },
      { type: 'areEqual', message: 'the password is not match' }
    ]
    ,
    'image': []
    // ,
    // 'dob': [
    //   { type: 'required', message: 'the date of birth is required' }
    // ]
  };

  //set upload image file 
  uploadEndpoint: string;
  uploadedUrl: string;
  progress: number;
  student: Student;
  isUploaded = false;
  isRegistering = false;

  ngOnit() {
    this.uploadEndpoint = environment.uploadApi;
  }
  //set button clicke to choose file 
  onUploadClicked(files?: FileList) {
    console.log(typeof (files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
              this.isUploaded = true;
            }, 1000);
        }
      });
  }
  onSelectedFilesChanged(files?: FileList) { }

  //maching confirmation password 
  matchPassword() {
    let password = this.form.get('password').value;
    let passwordConf = this.form.get('passwordConf').value;
    if (password != passwordConf) {
      this.form.get('passwordConf').setErrors({
        MatchPassword: true
      })
    }
  }

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private fileUploadService: FileUploadService,
    private studentService: StudentRestImplService,
    private authenService: AuthenticationService) { }


  //submit the student information of registeration
  submit() {
    console.log("submitted");
    const user = this.authenService.getCurrentUser();
      user.name = this.form.value.name;
      user.surname = this.form.value.surname;
      user.dob = this.form.value.dob;
      user.image = this.uploadedUrl;
    // this.student = new Student();
    // this.student.user = new User();
    // this.student.name = this.form.value.name;
    // this.student.surname = this.form.value.surname;
    // // this.student.studentId = this.form.value.studentId;
    // // this.student.dob = this.form.value.dob;
    // this.student.image = this.form.value.image;
    // this.student.user.username = this.form.value.email;
    // this.student.user.password = this.form.value.password;
    // this.student.user.firstname = this.form.value.name;
    // this.student.user.lastname = this.form.value.surname;
    // this.student.user.email = this.form.value.email;
    // this.student.user.enabled = true;
    // console.log(this.form.value.dob);
    this.studentService.saveStudent(user)
      .subscribe((user) => {
        this.authenService.setLocalUser(user);
        this.isRegistering = true;
        // var isClick = alert("You are registered to the system. \nPlease wait for the administrator to approve you.");
        this.router.navigate(['/student-dashboard']);
        window.location.reload();
      }, (error) => {
        alert("Could not save value");
      });
  }

  onClick() {
    alert("Register Successful");
  }

}
