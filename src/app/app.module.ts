import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatSnackBarModule,
  MatDialogModule,
  MatDatepickerModule,
  MatOptionModule,
  MatSelectModule,
  MatNativeDateModule,
  MatRadioModule
} from '@angular/material';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { MatFileUploadModule } from "mat-file-upload"
import { MatInputModule } from '@angular/material';
import { MyNavComponent, RegisterFormComponent } from './my-nav/my-nav.component';
import { NgModule } from '@angular/core';
import { StudentRestImplService } from './service/student-rest-impl.service';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentService } from './service/student-service';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsComponent } from './students/list/students.component';
// import { StudentsFileImpService } from './service/students-file-impl.service';
import { StudentsViewComponent } from './students/view/students.view.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './service/authentication.service';
import { JwtInterceptorService } from './helpers/jwt-interceptor.service';
import { TeacherComponent } from './teacher/teacher.component';
import { AdminComponent } from './admin/admin.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AdminStudentTableComponent } from './admin/admin-student-table/admin-student-table.component';
import { StudentDashboardComponent } from './students/student-dashboard/student-dashboard.component';
import { StudentEnrollmentComponent } from './students/student-enrollment/student-enrollment.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { TeacherListComponent } from './teacher/teacher-list/teacher-list.component';
import { ActivityService } from './service/activity-service';
import { ActivityRestImplService } from './service/activity-rest-impl.service';
import { SimpleDialogComponent } from './simple-dialog/simple-dialog.component';
import { CommentComponent } from './students/comment/comment.component';
import { StudentRegisterDialogComponent } from './students/student-register-dialog/student-register-dialog.component';
import { TeacherApproveComponent } from './teacher/teacher-approve/teacher-approve.component';
import { UpdateProfileDialogComponent } from './update-profile-dialog/update-profile-dialog.component';
import { HomeComponent } from './home/home.component';
import { ActivityUpdateComponent } from './teacher/activity-update/activity-update.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    LoginComponent,
    TeacherComponent,
    AdminComponent,
    AdminDashboardComponent,
    AdminStudentTableComponent,
    StudentDashboardComponent,
    StudentEnrollmentComponent,
    TeacherDashboardComponent,
    TeacherListComponent,
    RegisterFormComponent,
    SimpleDialogComponent,
    CommentComponent,
    StudentRegisterDialogComponent,
    TeacherApproveComponent,
    UpdateProfileDialogComponent,
    HomeComponent,
    ActivityUpdateComponent
  ],

  imports: [
    BrowserModule,
    MatDialogModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatProgressBarModule,
    StudentRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatFileUploadModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentRestImplService },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true },
    { provide: ActivityService, useClass: ActivityRestImplService }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    // RegisterFormComponent,
    SimpleDialogComponent,
    StudentRegisterDialogComponent,
    UpdateProfileDialogComponent
  ],

})
export class AppModule { }
