import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { error } from 'util';
import { StudentRegisterDialogComponent } from '../students/student-register-dialog/student-register-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  addressForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required]
  });
  lecturerId: number;

  validation_messages = {
    'username': [
      { type: 'required', message: 'username is <strong>required</strong>' },
    ],
    'password': [
      { type: 'required', message: 'the password is <strong>required</strong>' }
    ]
  };
  hide = true;

  returnUrl: string;
  isError = false;
  home: Boolean;

  constructor(private fb: FormBuilder, private router: Router,
    private authenService: AuthenticationService, private route: ActivatedRoute, public dialog: MatDialog) { }


    openStudentRegisterDialog() {
    this.dialog.open(StudentRegisterDialogComponent, {
         width: '90rem'
    });
}

  ngOnInit(): void {
    // reset login status
    this.authenService.logout();
    this.isError = false;
    //get return url from route parameter or deefault
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    if (this.returnUrl === '/') {
      this.home = true;
    } else {
      this.home = false;
    }
    console.log('return url' + this.returnUrl);
  }

  // onSubmit() {
  //   const value = this.addressForm.value;
  //   this.authenService.login(value.username, value.password)
  //     .subscribe(data => {
  //       this.isError = false;

  //       console.log(this.authenService.role());

  //       this.router.navigate([this.returnUrl]);
  //     },
  //     error => {
  //       this.isError = true;
  //     }
  //   );
  //   console.log(this.addressForm.value);
  // }
  
  onSubmit() {
    const value = this.addressForm.value;
    this.authenService.login(value.username, value.password)
      .subscribe(data => {
        this.isError = false;
        console.log(this.authenService.role());
        if (this.home == true) {
          // login by STUDENT role
          if (this.authenService.role() == "STUDENT") {
            this.router.navigate(['student-dashboard'])
            //login by TEACHER and ADMIN  role
          } else if(this.authenService.role() == "TEACHER") {
            this.router.navigate(['teacher-dashboard'])
          } else if(this.authenService.role() == "ADMIN") {
            this.router.navigate(['admin-dashboard'])
          } 
        }else{
          this.router.navigate([this.returnUrl]);
        }
      },
        error => {
          this.isError = true;
        }
      );
    console.log(this.addressForm.value);
  }

}
