import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Student from '../entity/student';
import { StudentService } from '../service/student-service';
import { AuthenticationService } from '../service/authentication.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { UpdateProfileDialogComponent } from '../update-profile-dialog/update-profile-dialog.component';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  defaultImageUrl = 'assets/images/camt.jpg';
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private studentService: StudentService
    , private authService: AuthenticationService, public dialog: MatDialog) { }
  hasRole(role: string) {
    return this.authService.hasRole(role);
  }
  get user() {
    return this.authService.getCurrentUser();
  }

  students$: Observable<Student[]> = this.studentService.getStudents();

  openSimpleDialog() {
    const myDialog = this.dialog.open(RegisterFormComponent, {
      width: '80rem'
    })
  }

  openUpdateProfileDialog() {
    const myDialog2 = this.dialog.open(UpdateProfileDialogComponent, {
      width: '80rem'
    })
  }
  get role(){
    const role = this.authService.role();
    if (role == "LECTURER"){
      return "TEACHER";
    }else{
      return role;
    }
  }


}
@Component({
  selector: 'app-register-form',
  templateUrl: 'register-form/register-form.component.html',
  styleUrls: ['register-form/register-form.component.css']
})
export class RegisterFormComponent {

  constructor(private snackbar: MatSnackBar) { }
  openSnackBar() {
    this.snackbar.open('Update Completed', '', {
      duration: 3000
    });
  }

}
