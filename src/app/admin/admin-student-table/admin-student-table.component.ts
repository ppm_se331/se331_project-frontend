import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from 'src/app/service/student-service';
import { StudentTableDataSource } from 'src/app/students/student-table/student-table-datasource';
import { StudentRestImplService } from 'src/app/service/student-rest-impl.service';
import { AdminStudentTableApprove } from './admin-student-table-datasourse';
@Component({
  selector: 'app-admin-student-table',
  templateUrl: './admin-student-table.component.html',
  styleUrls: ['./admin-student-table.component.css']
})
export class AdminStudentTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: AdminStudentTableApprove;
  loading: boolean;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id',
    'studentId',
    'name',
    'surname',
    'image',
    // 'gpa',
    'enrollButton'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private studentService: StudentService,private studentRestService: StudentRestImplService) { }
  ngOnInit() {
    this.loading = true;
    this.studentService.getStudents()
      .subscribe(students => {
        this.dataSource = new AdminStudentTableApprove();
        this.dataSource.data = students;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        setTimeout(() => {
          this.loading = false;
        }, 500);

      }
      );

      
  }

  ngAfterViewInit() {
  }

  approve(student: Student) {
      this.studentRestService.approveStudent(student.id).subscribe((student) => {
        console.log(student);
      });
      this.refresh();
      console.log(student.name + " " + student.surname + " has been approved!");
      
  }
  reject(student: Student) {
      console.log(student.id);
      this.studentRestService.rejectStudent(student.id).subscribe((data) => console.log(data));
      this.refresh();
      console.log(student.name + " " + student.surname + " has been reject!");
    }

    refresh(){
      this.studentRestService.getWaitingStudent()
        .subscribe(data => {
          this.dataSource = new AdminStudentTableApprove();
          this.dataSource.data = data;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.table.dataSource = this.dataSource;
          this.students = data;
  
        });
    }

}
