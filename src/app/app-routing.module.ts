import { StudentDashboardComponent } from './students/student-dashboard/student-dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { LoginComponent } from './login/login.component';
import { StudentEnrollmentComponent } from './students/student-enrollment/student-enrollment.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { TeacherApproveComponent } from './teacher/teacher-approve/teacher-approve.component';
import { AdminStudentTableComponent } from './admin/admin-student-table/admin-student-table.component';
import { CommentComponent } from './students/comment/comment.component';
import { StudentAndteacherGuard } from './Guard/student-and-lecturer.guard';
import { AdminGuard } from './Guard/admin.guard';
import { HomeComponent } from './home/home.component';
import { ActivityUpdateComponent } from './teacher/activity-update/activity-update.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'student-dashboard', component: StudentDashboardComponent},
  { path: 'teacher-dashboard', component: TeacherDashboardComponent},
  { path: 'admin-dashboard',component: TeacherDashboardComponent},
  { path: 'enrollment', component: StudentEnrollmentComponent},
  { path: 'teacher-approve', component: TeacherApproveComponent},
  { path: 'login', component: LoginComponent},
  { path: 'student-approve', component: AdminStudentTableComponent},
  { path: 'update-activity', component: ActivityUpdateComponent},
  { path: 'home', component: HomeComponent },
  { path: 'DescriptionActivity/:id', component: CommentComponent ,canActivate:[StudentAndteacherGuard]},
  { path: '**', component: FileNotFoundComponent },

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
