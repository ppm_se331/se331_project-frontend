import { User } from './user';

export class Person {
    id: number;
    name: string;
    surname: string;
    user: User;
    dob: Date;
    image: string;
}
