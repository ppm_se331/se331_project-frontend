import { Time } from '@angular/common'
import { logging } from 'protractor';
import Student from './student';
import { Comment } from './comment';
import Teacher from './teacher';

export default class Activity {
    id: number;
    activityId: number;
    activityName: string;
    activityImage: string;
    date: string;
    location: string;
    description: string;
    regisDateStart: string;
    regisDateEnd: string;
    time: string;
    host: string;
    comments: Comment[];
    students:Student[];
    teacher:Teacher;
    enrolledStudent: Student[];
    pendingActivities: Student[];


  }
  