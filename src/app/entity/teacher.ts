export default class Teacher {
    TeacherId: number;
    TeacherName: string;
    TeacherSurname: string;
    dob:string;
    username: string;
    email: string;
    password: string;
  }
  