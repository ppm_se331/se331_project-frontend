import { Authority } from './authority';
import { Person } from './person';

export class User {
    id: number;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    email: string;
    enabled: Boolean;
    lastPasswordResetDate: Date;
    authorities: Authority[];
    appUser: Person;
}
