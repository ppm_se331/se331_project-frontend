import Activity from './activity';
import { User } from './user';

export default class Student {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  image: string;
  user: User;
  username: string;
  email: string;
  password: string;
  enrolledActivities: Activity[];
  pengdingActivity: Activity[];
  comment:string;
  featured: boolean;
  // dob:string;
}
