import { User } from './user';
import Activity from './activity';

export class Comment {
    id: number;
    text: String;
    image: String;
    user: User;
    activity: Activity;
    userImage: string;
}
