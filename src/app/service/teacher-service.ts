import { Observable } from '../../../node_modules/rxjs';
import Teacher from '../entity/teacher';

export abstract class TeacherService {
    abstract getTeachers(): Observable<Teacher[]>;
    abstract getTeacher(id: number): Observable<Teacher>;
    abstract saveTeacher(student: Teacher): Observable<Teacher>;
}