import { TestBed } from '@angular/core/testing';

import { ActivityRestImplService } from './activity-rest-impl.service';

describe('ActivityRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityRestImplService = TestBed.get(ActivityRestImplService);
    expect(service).toBeTruthy();
  });
});
