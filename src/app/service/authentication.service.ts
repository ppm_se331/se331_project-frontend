import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
 
  constructor(private http: HttpClient) { }
  login(username: string, password: string){
    return this.http.post<any>(environment.authenticatinApi,
      {username: username,password: password})
      .pipe(map(data => {
        if(data.token && data.user){
          localStorage.setItem('token',data.token);
          localStorage.setItem('currentUser',JSON.stringify(data.user));
        }
        return data;
      }));
  }
  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
  }
  getToken(): string {
    const token = localStorage.getItem('token');
    return token ? token : '';
  }
  signUp(username: string, password: string){
    this.http.post<any>(environment.authenticatinApi,
    {username: username, password: password})
  }

  getCurrentUser() {
    const currentUser = localStorage.getItem('currentUser');
    if (currentUser) {
      return JSON.parse(currentUser);
    } else {
      return null;
    }
  }

  hasRole(role: string): boolean {
    const user: any = this.getCurrentUser();

    if (user) {
      const roleList: string[] = role.split(',');
      for (let j = 0; j < roleList.length; j++) {
        const authList = user.authorities;
        const userRole = 'ROLE_' + roleList[j].trim().toUpperCase();
        for (let i = 0; i < authList.length; i++) {
          if (authList[i].name === userRole) {
            return true;
          }
        }
      }     
    }
    return false;
  }

  role(){
    const user:any = this.getCurrentUser();
    const role = user.authorities[0].name.substr(5);
    return role;
  }
  
  setLocalUser(data){
    if(data.student){
      data.user.studentId = data.student.studentId;
      data.user.dob = new Date(data.user.dob);
      localStorage.setItem('currentUser', JSON.stringify(data.user));
    } else {
      data.dob = new Date(data.dob)
      localStorage.setItem('currentUser', JSON.stringify(data));
    }   
  }
}
