import { Injectable } from '@angular/core';
import { ActivityService } from './activity-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Activity from '../entity/activity';
import { environment } from 'src/environments/environment';
import { Comment } from 'src/app/entity/comment';
import Student from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class ActivityRestImplService extends ActivityService {

  getAllActivities(): Observable<Activity[]>  {
    return this.http.get<Activity[]>(environment.activityApi)
  }
  getActivity(id: number): Observable<Activity>{
    return this.http.get<Activity>(environment.activityApi + '/'+ id)
  }
  saveActivity(activity: Activity): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi, Activity);
  }
  approveStudent(actId: number, stuId: number): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi + "/" + actId + "/approve/" + stuId, {});
  }
  rejectStudent(actId: number, stuId: number): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi + "/" + actId + "/reject/" + stuId, {});
  }
  getActivityWaitingStudent(id: number): Observable<Student[]> {
    return this.http.get<Student[]>(environment.activityApi + '/students/' + id);
  }
  addComment(comment: Comment): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi + '/comment', comment);
  }
  deleteComment(comment: Comment): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi + "/comment/delete/" + comment.id, {});
  }
  
  constructor(private http: HttpClient) {
    super();
   }


  
}
