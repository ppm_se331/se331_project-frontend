import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import Activity from '../entity/activity';

@Injectable({
  providedIn: 'root'
})
export class StudentRestImplService extends StudentService {

  constructor(private http: HttpClient) {
    super();
  }
  updateStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi,student)
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, student);
  }

  getStudentActivities(id: number): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.studentApi + '/' + id + "/activity");
  }

  getActivityPendingStudent(id: number): Observable<Student[]> {
    return this.http.get<Student[]>(environment.activityApi + '/' + id + '/student');
  }

  approveStudent(id: number): Observable<Student> {
    return this.http.post<Student>(environment.studentApi + '/approve/', id);
  }
  rejectStudent(id: number) {
    return this.http.post(environment.studentApi + '/reject/', id);
  }
  getWaitingStudent(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi + '/studentswaitinglist' );
  }

}
