import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Image } from '../entity/Image';

@Injectable({
    providedIn: 'root'
  })

export  class UserService {
    constructor(private http: HttpClient) { }
    // abstract getUsers(): Observable<User[]>;
    // abstract getWaitingForAprrovalUsers(): Observable<User[]>;
    getUserImageByUserId(id: number): Observable<Image>{
        return this.http.get<Image>(environment.userApi + "/" + id)
      }
}
