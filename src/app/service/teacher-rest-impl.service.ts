import { Injectable } from '@angular/core';
import { TeacherService } from './teacher-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import Teacher from '../entity/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherRestImplService extends TeacherService{
  getTeachers(): Observable<Teacher[]>{
    return this.http.get<Teacher[]>(environment.TeacherApi)
  }
  getTeacher(id: number): Observable<Teacher>{
    return this.http.get<Teacher>(environment.TeacherApi+ '/' + id)
  }
  saveTeacher(teacher: Teacher): Observable<Teacher>{
    return this.http.post<Teacher>(environment.TeacherApi, Teacher);
  }

  constructor(private http: HttpClient) {
    super();
   }
}
