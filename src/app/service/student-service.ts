import { Observable } from '../../../node_modules/rxjs';
import Student from '../entity/student';
import Activity from '../entity/activity';

export abstract class StudentService {
    abstract getStudents(): Observable<Student[]>;
    abstract getStudent(id: number): Observable<Student>;
    abstract saveStudent(student: Student): Observable<Student>;
    abstract updateStudent(student: Student): Observable<Student>;
    // abstract getStudentActivities(id: number): Observable<Activity[]>
    // abstract getActivityPendingStudent(id: number): Observable<Student[]>
    // abstract getWaitingStudent(): Observable<Student[]>
    // abstract approveStudent(id: number): Observable<Student>
    // abstract rejectStudent(id: number)
}
