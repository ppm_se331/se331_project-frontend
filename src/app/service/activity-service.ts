import { Observable } from '../../../node_modules/rxjs';
import Activity from '../entity/activity';

export abstract class ActivityService {
    abstract getAllActivities(): Observable<Activity[]>;
    abstract getActivity(id : number): Observable<Activity>;
    abstract saveActivity(activity: Activity): Observable<Activity>;
    abstract approveStudent(actId: number, stuId: number): Observable<Activity>;
    abstract rejectStudent(actId: number, stuId: number): Observable<Activity>;

}
