import { TestBed, async, inject } from '@angular/core/testing';

import { StudentAndteacherGuard } from './student-and-lecturer.guard';

describe('StudentAndteacherGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentAndteacherGuard]
    });
  });

  it('should ...', inject([StudentAndteacherGuard], (guard: StudentAndteacherGuard) => {
    expect(guard).toBeTruthy();
  }));
});
