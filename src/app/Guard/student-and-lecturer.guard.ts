import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';


@Injectable({
  providedIn: 'root'
})
export class StudentAndteacherGuard {
  constructor(private router:Router,private authService:AuthenticationService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
      if (this.authService.hasRole('STUDENT') ||
        this.authService.hasRole('TEACHER')){
          return true;
        }
        if (this.authService.hasRole('ADMIN')
          ){
            return true;
          }
        
        this.router.navigate(['/login'],{queryParams: {returnUrl: state.url}
    });
    return false;
  }
}
